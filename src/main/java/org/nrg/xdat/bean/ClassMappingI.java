package org.nrg.xdat.bean;

import java.util.Map;

public interface ClassMappingI {

	public abstract Map<String, String> getElements();

}