package org.nrg.xdat.security.user.exceptions;

public class PasswordComplexityException extends Exception {

    public PasswordComplexityException(String message) {
        super(message);
    }
}