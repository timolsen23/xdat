package org.nrg.xdat.security.user.exceptions;

public class UserFieldMappingException extends Exception{
	public UserFieldMappingException(Exception e){
		super(e);
	}
}