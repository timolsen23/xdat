/*
 * org.nrg.xdat.turbine.modules.screens.XDATScreen_report_xdat_userGroup
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:13 AM
 */


package org.nrg.xdat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class XDATScreen_report_xdat_userGroup extends SecureReport {

    @Override
    public void finalProcessing(RunData data, Context context) {
        
    }

}
